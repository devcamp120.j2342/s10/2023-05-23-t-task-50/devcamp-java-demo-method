public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");      
        int sum = App.sumNumbersV1();
        System.out.println("Tổng 100 số tự nhiên = " + sum);
        App app = new App();
        int sum2 = app.sumNumbersV11();
        System.out.println("Tổng 100 số tự nhiên = " + sum2);

        int[]number1 = {1, 5, 10};
        int sum3 = sumNumbersV2(number1);
        System.out.println("Tổng của mảng number1 = " + sum3);

        printHello(-10);
        printHello(24);
        printHello(99);
    }


    //Tính tổng 100 số tự nhiên đầu tiên
    public static int sumNumbersV1() {
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            //sum = sum + i;
            sum += i;
        }

        return sum;
    }

    public int sumNumbersV11() {
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            //sum = sum + i;
            sum += i;
        }

        return sum;
    }

    //Tính tổng mảng số nguyên
    public static int sumNumbersV2(int[] arrayNumber) {
        int sum = 0;
        for (int i = 0; i < arrayNumber.length; i++) {
            sum += arrayNumber[i];
        }
        return sum;
    }

    //Hiển thị thông báo số truyền vào
    public static void printHello(int number) {
        if (number <= 0) {
            System.out.println("Đây là số nhỏ hơn hoặc bằng 0");
        } else {
            if (number % 2 == 0) {
                System.out.println("Đây là số chẵn!");
            } else {
                System.out.println("Đây là số lẻ!");
            }
        }
    }
}
